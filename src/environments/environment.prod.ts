export const environment = {
  production: true,

  // Configure Variables
  appURL: 'https://app.castlecraft.in',
  authorizationURL: 'https://accounts.castlecraft.in/oauth2/confirmation',
  authServerURL: 'https://accounts.castlecraft.in',
  callbackProtocol: 'castlecraft',
  callbackUrl: 'https://app.castlecraft.in/connected_device/callback',
  clientId: 'client_id',
  profileURL: 'https://accounts.castlecraft.in/oauth2/profile',
  revocationURL: 'https://accounts.castlecraft.in/oauth2/revoke',
  scope: 'profile openid email roles phone',
  tokenURL: 'https://accounts.castlecraft.in/oauth2/token',
  isAuthRequiredToRevoke: true,
};

// client_id is available for public, client_secret is to be protected.
