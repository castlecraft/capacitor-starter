# For Electron

- https://capacitor-community.github.io/electron/#/./getting-started

package.json

```json
{
  ...
    "scripts": {
    ...
    "electron:build-linux": "npm run build && electron-builder build --linux"
  },
  "build": {
    ...
    "files": [
      ...
      "user-preload-script.js"
    ],
    "protocols": [
      {
        "name": "Awesome App",
        "schemes": [
          "castlecraft"
        ]
      }
    ],
    ...
    "linux": {
      "target": "AppImage",
      "icon": "assets/appIcon.png"
    }
  }
}
```

tsconfig.json

```json
{
  ...
  "compilerOptions": {
    ...
    "typeRoots": ["node_modules/@types"],
    "types": ["node"]
  }
}

```

user-preload-script.js

```js
window.nodeRequire = require;
delete window.require;
delete window.exports;
delete window.module;
```

index.ts

```ts
...
import {
  createCapacitorElectronApp,
  createCapacitorElectronDeepLinking,
} from "@capacitor-community/electron";

// The MainWindow object can be accessed via myCapacitorApp.getMainWindow()
const capacitorApp = createCapacitorElectronApp({
  splashScreen: { useSplashScreen: false },
  mainWindow: { windowOptions : { autoHideMenuBar: true } },
});

export const customProtocol = 'castlecraft';
createCapacitorElectronDeepLinking(capacitorApp, { customProtocol });

...

// Login pop-over
app.on('browser-window-created', (event, win) => {
  if (win.getTitle() === '_self') {
    win.webContents.addListener('did-redirect-navigation', (event, url) => {
      if (url.includes(`${customProtocol}://`)) {
        const slug = url.split(`${customProtocol}://`).pop();
        capacitorApp.getMainWindow().loadURL(`capacitor-electron://-/${slug}`)
        win.close();
      }
    });
  }
});

```
